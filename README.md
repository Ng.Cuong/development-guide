# HORUSOFTACEAE GUIDELINES

Welcome to HORUSOFTACEAE! This is the first thing you should read when working with us. This document contains the guidelines for writing code, commit messages, and documentation. Please read it carefully and follow the guidelines.

## Basic rules

Please follow these basic rules when working with us

-   [Git & Commit Guidelines](./guidelines/git-guidelines.md)

## Code Guidelines

-   [React Guidelines](./section/REACTJS.md)

## Author

-   [Horusoftaceae](https://horusoftaceae.com/) with [email](mailto:support@horusoftaceae.com)
-   [Arceus](mailto:nguyen.cuong@horus.com.vn)
-   [Duy Le](mailto:le.duy@horus.com.vn)
