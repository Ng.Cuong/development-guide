# Git & commit guidelines

-   This document define how to use Git and write commit messages. Please read it carefully and follow the guidelines.
-   See all guidelines [here](../README.md).

## Table of contents

-   [Commit message format](#commit-message-format)
-   [Branch naming convention](#branch-naming-convention)

## Commit message format

Base on [Conventional Commits](https://www.conventionalcommits.org/), we have some rules to follow when writing commit messages.

A commit message consists of a header, body, and footer. The header has a special format that includes a type, scope, and subject:

```
<type>(<scope>): <subject>

<body>

<footer>
```

-   The type is required and specifies the type of change that you're committing.

    -   `fix`: Represents a bug fix for your application.
    -   `feat`: Adds a new feature to your application or library.
    -   `refactor`: A code change that neither fixes a bug nor adds a feature.
    -   `deploy`: Changes to modify or related to the deployment process.
    -   `chore`: Upgrades libraries and/or performs maintenance tasks.
    -   `docs`: Documentation only changes.
    -   `test`: Adding missing tests or correcting existing tests.
    -   `style`: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc).

-   The scope is optional and specifies the place of the commit change. All scope must be define when start a project, and must not be change.

-   The subject is required and contains a succinct description of the change.

-   The body is optional and contains a more detailed description of the change.

-   Footer only use when task is complete, and contains information about task id, with the format `QMS-<task-id>`

-   We highly recommend using the `Conventional Commits Extension` if you're using Visual Studio Code.

## Branching model

We use the `Gitflow` branching model. Please read more about it [here](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).
