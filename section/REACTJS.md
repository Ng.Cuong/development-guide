# ReactJs guidelines

-   Some guidelines for developing that we should follow.
-   See all guidelines [here](../README.md).

## Table of Contents

-   [Components](#components)
    -   [Com001: DON'T declare a variable without hook](#com001-dont-declare-a-variable-without-hook)
    -   [Com002: DON'T use `var` keyword](#com002-dont-use-var-keyword)
    -   [Com003: DON'T interact with DOM directly](#com003-dont-interact-with-dom-directly)
    -   [Com004: Every effect must be wrapped in `useEffect` hook](#com004-every-effect-must-be-wrapped-in-useeffect-hook)
    -   [Com005: DON'T use `onClick` event on `a` tag, `Link` & `NavLink` component or an component that render as `a` tag](#com005-dont-use-onclick-event-on-a-tag-link--navlink-component-or-an-component-that-render-as-a-tag)

## Components

#### Com001: DON'T declare a variable without hook

Any variable declared in a component must be constant and must be the result of a hook.

**Bad**

```jsx
const MyComponent = () => {
    let myVar = 0;
    return (
        <div>
            <span>{myVar}</span>
            <button onClick={() => myVar++}>Increment</button>
        </div>
    );
};
```

**Good**

```jsx
const MyComponent = () => {
    const [myVar, setMyVar] = useState(0);
    return (
        <div>
            <span>{myVar}</span>
            <button onClick={() => setMyVar(myVar + 1)}>Increment</button>
        </div>
    );
};
```

It make the code more readable and easier to debug. Also it helps to optimize the component.

**_Exceptions_**

-   The boolean variable that is calculated from logical operations can be declared without hook. In this case, the variable must be declared as `const` and don't use `useMemo` or `useState` hook.

```jsx
const MyComponent = ({ activeItemUid, uid }) => {
    const isActive = activeItemUid === uid;
    return <div className={isActive ? "active" : ""}>...</div>;
};
```

-   The variable is deconstructed from props can be declared without hook. In this case, `useMemo` hook is not necessary.

```jsx
const MyComponent = () => {
    const userProfile = useUserProfile();
    const { name, age } = userProfile;
    return (
        <div>
            <span>{name}</span>
            <span>{age}</span>
        </div>
    );
};
```

### Com002: DON'T use `var` keyword

The `var` keyword is not block scoped, so it can be used outside the block where it was declared.
This can lead to unexpected behavior.

**Bad**

Any code that uses `var` keyword.

**Good**

Any code that not bad.

**_Exceptions_**

No exceptions.

### Com003: DON'T interact with DOM directly

Interact with DOM directly can lead to unexpected behavior because React can't control the DOM.

**Bad**

```jsx
const MyComponent = () => {
    const setActive = useCallback(() => {
        const span = document.getElementById("my-span");
        span.classList.remove("in-active");
        span.classList.add("active");
    }, []);

    return (
        <div>
            <span id="my-span" className="in-active">
                ...
            </span>
            <button onClick={setActive}>Set active</button>
        </div>
    );
};
```

**Good**

```jsx
const MyComponent = () => {
    const [isActive, setIsActive] = useState(false);
    const setActive = useCallback(() => {
        setIsActive(true);
    }, []);

    return (
        <div>
            <span className={isActive ? "active" : "in-active"}>...</span>
            <button onClick={setActive}>Set active</button>
        </div>
    );
};
```

### Com004: Every effect must be wrapped in `useEffect` hook

Wrap every effect in `useEffect` hook to avoid unexpected and redundant behavior.

**Bad**

```jsx
const MyComponent = () => {
    // do something

    return <div>...</div>;
};
```

**Good**

```jsx
const MyComponent = () => {
    useEffect(() => {
        // do something
    }, []);

    return <div>...</div>;
};
```

### Com005: DON'T use `onClick` event on `a` tag, `Link` & `NavLink` component or an component that render as `a` tag

Logic of `a` tag is jump to another page, so it should not have `onClick` event. It can lead to unexpected behavior and make the code harder to debug.

**Bad**

```jsx
const MyComponent = () => {
    const handleClick = useCallback(() => {
        // do something
    }, []);

    return (
        <a href="#" onClick={handleClick}>
            ...
        </a>
    );
};
```

**Good**

```jsx
const MyComponent = () => {
    const pathnameChanged = usePathnameChanged();

    useEffect(() => {
        if (pathnameChanged) {
            // do something
        }
    }, [pathnameChanged]);

    return <a href="#">...</a>;
};
```

### Com006: DON'T use nested ternary operator

Nested ternary operator make the code harder to read and debug.

**Bad**

```jsx
const MyComponent = ({ errors, warnings, infos }) => {
    return (
        <div
            className={
                errors.length > 0
                    ? "error"
                    : warnings.length > 0
                    ? "warning"
                    : infos.length > 0
                    ? "info"
                    : ""
            }
        >
            ...
        </div>
    );
};
```

**Good**

```jsx
const MyComponent = ({ errors, warnings, infos }) => {
    let className = "";
    if (errors.length > 0) {
        className = "error";
    } else if (warnings.length > 0) {
        className = "warning";
    } else if (infos.length > 0) {
        className = "info";
    }

    return <div className={className}>...</div>;
};
```
